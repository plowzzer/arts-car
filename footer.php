<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package arts_car
 */

?>

<footer>
	<div class="container">
		<div class="col-xs-12 col-md-2 logo">
			<a <?php if ( is_page ( 'home' ) || is_singular( 'home' ) ) { ?> class="ative"<?php } ?> href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?=bloginfo('stylesheet_directory')?>/assets/image/logo.png" alt="Arts Car" /></a>
		</div>
		<div class="col-xs-12 col-md-3 address">
			<p>Av. Maria de Jesus Condeixa, nº 800<br>
			Jd. Palma Travassos - Ribeirão Preto - SP</p>
		</div>
		<div class="col-xs-12 col-md-2 phone">
			<p><span>16</span> 3289.8328</p>
		</div>
		<div class="col-xs-12 col-md-4 faceplug">
			<div class="fb-page" data-href="https://www.facebook.com/artscarmartelinhodeourorp" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/artscarmartelinhodeourorp"><a href="https://www.facebook.com/artscarmartelinhodeourorp">Arts Car Martelinho de Ouro</a></blockquote></div></div>
		</div>
		<div class="col-xs-12 col-md-1 oneicon">
			<a href="http://onegate.com.br/"><img src="<?=bloginfo('stylesheet_directory')?>/assets/image/onegate.png" alt="Onegate" /></a>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>


<!-- Facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=212123358849750";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-72893712-1', 'auto');
  ga('send', 'pageview');
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '180773545662446');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=180773545662446&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->


</body>
</html>
