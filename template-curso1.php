<?php
/**
*
*Template Name: Curso Martelinho de Ouro
*Template texto: Pagina de Curso 1
*
* @package arts_car
*/

get_header();?>
<div class="navegation">
  <li class="col-xs-6 col-md-3"><a href="#van" data-uk-smooth-scroll="{offset: 100}">Vantagens</a></li>
  <li class="col-xs-6 col-md-3"><a href="#vid" data-uk-smooth-scroll="{offset: 100}">Vídeos</a></li>
  <li class="col-xs-6 col-md-3"><a href="#mod" data-uk-smooth-scroll="{offset: 100}">Modalidades</a></li>
  <li class="col-xs-6 col-md-3"><a href="#con" data-uk-smooth-scroll="{offset: 100}">Contato</a></li>
</div>

  <div class="page_course">
    <div id="van" class="advantages">
      <h1 class="call"><span>Vantagens</span></h1>
      <div class="container">
        <div class="advantage col-xs-6 col-md-4">
          <img src="<?=bloginfo('stylesheet_directory')?>/assets/cursos/material.png" alt="Material Didático" />
          <h1>Material Didático</h1>
          <p>Todas as ferramentas e materiais para treinamento são fornecidos pela Arts Car. No final do curso o aluno poderá montar e adquirir seu kit personalizado.</p>
        </div>
        <!-- <div class="advantage col-xs-6 col-md-4">
          <img src="<?=bloginfo('stylesheet_directory')?>/assets/cursos/avaliacao.png" alt="Avaliação" />
          <h1>Avaliação</h1>
          <p>A cada etapa o aluno será avaliado para então dar sequencia ao conteúdo programático.</p>
        </div> -->
        <div class="advantage col-xs-6 col-md-4">
          <img src="<?=bloginfo('stylesheet_directory')?>/assets/cursos/certificado.png" alt="Certificação" />
          <h1>Certificação</h1>
          <p>Certificado de conclusão do curso que atesta a participação do aluno.</p>
        </div>
        <div class="advantage col-xs-6 col-md-4">
          <img src="<?=bloginfo('stylesheet_directory')?>/assets/cursos/mercado.png" alt="Mercado de Trabalho" />
          <h1>Mercado de Trabalho</h1>
          <p>O profissional de martelinho de ouro está capacitado para atuar em amplo mercado de trabalho. Destacamos as principais atividades a seguir:<br>
            1 – Abrir oficina própria.<br>
            2 – Serviços externos: o profissional poderá realizar o serviço em domicílio, em estacionamentos, em lava-rápidos, em concessionárias, em garagens multimarcas, em oficinas automotivas, entre outros.<br>
            3 – Colaborador/Freelancer: em oficinas automotivas ou concessionárias homologadas das maiores montadoras do Brasil.<br>
            4 – Granizeiro:<br>
            *Granizeiro: em missão especial nacional ou internacional, para reparar danos na lataria ocasionados pele chuva de granizo.</p>
        </div>
        <div class="advantage col-xs-6 col-md-4">
          <img src="<?=bloginfo('stylesheet_directory')?>/assets/cursos/financeiro.png" alt="Potencial Ganho" />
          <h1>Potencial Ganho</h1>
          <p>O salário de um profissional de martelinho de ouro pode variar entre R$3.000,00 e R$8.000,00.</p>
        </div>
        <!-- <div class="advantage col-xs-6 col-md-4">
          <img src="<?=bloginfo('stylesheet_directory')?>/assets/cursos/retorno.png" alt="Retorno sobre Investimento" />
          <h1>Retorno sobre Investimento</h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vitae risus arcu. Etiam tincidunt venenatis odio, sed dignissim odio finibus alorem nec arcu.</p>
        </div> -->
        <!-- <div class="advantage col-xs-6 col-md-4">
          <img src="<?=bloginfo('stylesheet_directory')?>/assets/cursos/alojamento.png" alt="Alojamento" />
          <h1>Alojamento</h1>
          <p>A Arts Car oferece alojamento gratuito para o aluno.</p>
        </div> -->
        <div class="advantage col-xs-6 col-md-4">
          <img src="<?=bloginfo('stylesheet_directory')?>/assets/cursos/sistema.png" alt="Sistema de Ensino" />
          <h1>Sistema de Ensino</h1>
          <p>Curso prático e individual. Carga horária e conteúdo personalizados. Após completar o treinamento inicial em bancada, o aluno é promovido a trabalhar no veículo para vivenciar o dia a dia real de uma oficina. Desde o orçamento até a entrega do veículo.</p>
        </div>
        <div class="advantage col-xs-6 col-md-4">
          <img src="<?=bloginfo('stylesheet_directory')?>/assets/cursos/assistencia.png" alt="Assistência Pós-Curso" />
          <h1>Assistência Pós-Curso</h1>
          <p>O aluno poderá agendar treinamentos práticos monitorados, para o aperfeiçoamento da técnica. </p>
        </div>
      </div>

    </div>
    <!-- End of advantages -->

    <div id="vid" class="videos">
      <h1 class="call"><span>Vídeos</span></h1>
      <div class="container">
        <a href="https://www.youtube.com/watch?v=EM7dlHCujxw" data-uk-lightbox="{group:'my-group'}" data-lightbox-type="video"><img class="col-xs-4" src="<?=bloginfo('stylesheet_directory')?>/assets/cursos/arnaldo.jpg" alt="Depoimento Arnaldo" /></a>
        <a href="https://www.youtube.com/watch?v=FCD4JNpFVD4" data-uk-lightbox="{group:'my-group'}" data-lightbox-type="video"><img class="col-xs-4" src="<?=bloginfo('stylesheet_directory')?>/assets/cursos/fernando.jpg" alt="Depoimento Fernando" /></a>
        <a href="https://www.youtube.com/watch?v=BgWyG5iVLUs" data-uk-lightbox="{group:'my-group'}" data-lightbox-type="video"><img class="col-xs-4" src="<?=bloginfo('stylesheet_directory')?>/assets/cursos/video1.jpg" alt="Vídeo 1" /></a>
        <a href="https://www.youtube.com/watch?v=BgWyG5iVLUs" data-uk-lightbox="{group:'my-group'}" data-lightbox-type="video"><img class="col-xs-4" src="<?=bloginfo('stylesheet_directory')?>/assets/cursos/video2.jpg" alt="Vídeo 2" /></a>
        <a href="https://www.youtube.com/watch?v=BgWyG5iVLUs" data-uk-lightbox="{group:'my-group'}" data-lightbox-type="video"><img class="col-xs-4" src="<?=bloginfo('stylesheet_directory')?>/assets/cursos/video3.jpg" alt="Vídeo 3" /></a>
      </div>
    </div>
    <div id="mod" class="description">
      <h1 class="call"><span>Modalidade</span></h1>
      <!-- <div class="modality">
        <div class="container">
          <h2 class="title">EXPRESS / 60 horas</h2>
          <p>
            Recomendado para iniciantes no segmento automotivo que desejam conhecer a técnica e se aperfeiçoar com o passar do tempo.<br><br>
            <strong>Semanal:</strong> Segunda a sexta feira - 07:00 as 18:00hs + 10 horas de estágio.<br><br>
            <strong>Finais de Semana:</strong> Sábado, domingo, sábado, domingo e sábado - 07:00 as 18:00hs + 10 horas de estágio.<br>
          </p>
          <div class="col-xs-12 col-md-6 no-padding">
            <div class="col-xs-6 right-padding">
              <li>Palestra motivacional, mercado de trabalho e teoria da técnica</li>
              <li>Estudo da lataria do veículo</li>
              <li>Iluminação</li>
              <li>Pequenos amassados</li>
              <li>Pequenos amassados sob travessas</li>
            </div>
            <div class="col-xs-6 right-padding">
              <li>Sistema americano</li>
              <li>Chuva de granizo</li>
              <li>Acabamento</li>
              <li>Desmontagem e Montagem</li>
              <li>Orçamentação</li>
            </div>
          </div>
        </div>
        <div class="price">
          <h3>R$ 3.000,00 <span>à vista</span></h3>
          <h4>Ou 6x <strong>534,00*</strong> ou 10x de <strong>R$ 369,00*</strong></h4>
        </div>
      </div>

      <div class="modality">
        <div class="container">
          <h2 class="title">Express / 45 horas</h2>
          <p>
            Recomendado para iniciantes no segmento automotivo que desejam conhecer a técnica e se aperfeiçoar com o passar do tempo.            <srong>Semanal:</strong> Segunda a sexta-feira - 08:00 as 18:00hs.<br><br>
            <srong>Finais de Semana:</strong> Sábado, domingo, sábado, domingo e sábado - 07:00 as 18:00hs + 10 horas de estágio
          </p>
          <div class="col-xs-12 col-md-6 no-padding">
            <div class="col-xs-6 right-padding">
              <li>Palestra motivacional, mercado de trabalho e teoria da técnica</li>
              <li>Estudo da lataria do veículo</li>
              <li>Iluminação</li>
              <li>Pequenos amassados</li>
              <li>Pequenos amassados sob travessas</li>
              <li>Sistema americano</li>
            </div>
            <div class="col-xs-6 right-padding">
              <li>Chuva de granizo</li>
              <li>Acabamento</li>
              <li>Desmontagem e Montagem</li>
              <li>Orçamentação</li>
            </div>
          </div>
        </div>
        <div class="price">
          <h3>R$ 2.000,00 <span>à vista</span></h3>
          <h4>Ou 6x <strong>375,00*</strong> ou 10x de <strong>R$ 250,00*</strong></h4>
        </div>
      </div>

      <div class="modality">
        <div class="container">
          <h2 class="title">INTEGRAL / 120 horas</h2>
          <p>
            Formação integral teórica e prática do martelinho de ouro. Completo programa de ensinamentos, de treinamento de recuperação dos amassados.<br><br>
            Recomendado para os alunos que buscam iniciação ou recolocação profissional imediata.<br><br>
            <strong>Semanal:</strong> segunda a sexta-feira (duas semanas) - 07:00 as 18:00hs + 20 horas de estágio.
          </p>
          <div class="col-xs-12 col-md-6 no-padding">
            <div class="col-xs-6 right-padding">
              <li>Palestra motivacional, mercado de trabalho e teoria da técnica.</li>
              <li>Estudo da lataria do veículo</li>
              <li>Iluminação</li>
              <li>Pequenos amassados</li>
              <li>Pequenos amassados sob travessas Sistema americano</li>
              <li>Chuva de granizo</li>
              <li>Amassados médios</li>
            </div>
            <div class="col-xs-6 right-padding">
              <li>Amassados grandes</li>
              <li>Amassados sob travessas</li>
              <li>Vincos</li>
              <li>Vincos sob travessas</li>
              <li>Amassados em plásticos, alumínios e blindados.</li>
              <li>Acabamento</li>
              <li>Desmontagem e Montagem</li>
              <li>Orçamentação.</li>
            </div>
          </div>
        </div>
        <div class="price">
          <h3>R$ 5.000,00 <span>à vista</span></h3>
          <h4>ou 6x <strong>R$ 900,00*</strong> ou 10x de <strong>R$ 615,00</strong></h4>
        </div>
      </div> -->

      <div class="modality">
        <div class="container">
          <h2 class="title">INTEGRAL / Até 180 horas</h2>
          <p>
            O Aluno tem o período de 30 dias, em horário comercial, para realizar o curso, adaptando os dias e horário de acordo com sua disponibilidade.<br>
            Formação integral teórica e prática do martelinho de ouro. Completo programa de ensinamentos, de treinamento de recuperação dos amassados.<br>
            Recomendado para os alunos que buscam iniciação ou recolocação profissional imediata.
            <!-- <strong>Semanal:</strong> segunda a sexta-feira - 07:00 as 18:00hs + 10 horas de estágio.
            <strong>Finais de semana:</strong> sábado, domingo, sábado, domingo e sábado - 07:00 as 18:00hs + 10 horas de estágio. -->
          </p>
          <div class="col-xs-12 col-md-6 no-padding">
            <div class="col-xs-6 right-padding">
              <li>Palestra motivacional, mercado de trabalho e teoria da técnica.</li>
              <li>Estudo da lataria do veículo</li>
              <li>Iluminação</li>
              <li>Pequenos amassados</li>
              <li>Pequenos amassados sob travessas· Sistema americano</li>
              <li>Chuva de granizo</li>
              <li>Amassados médios </li>
            </div>
            <div class="col-xs-6 right-padding">
              <li>Amassados grandes</li>
              <li>Amassados sob travessas</li>
              <li>Vincos</li>
              <li>Vincos sob travessas.</li>
              <li>Acabamento</li>
              <li>Desmontagem e Montagem</li>
              <li>Orçamentação.</li>
            </div>
          </div>
        </div>
        <div class="price">
          <h3>R$ 4.500,00 <span>à vista</span></h3>
          <h4>Ou 6x <strong>833,00*</strong> ou 10x de <strong>R$ 550,00*</strong></h4>
        </div>
      </div>
    </div>

    <div id="con" class="contact">
      <h1 class="call"><span>Contato</span></h1>
      <div class="container">
        <?php $mail_field = rwmb_meta( 'ac-contact-form-course'); ?>
        <?php echo do_shortcode( $mail_field ); ?>
      </div>

    </div>
  </div>
<script>
$(document).ready(function() {
  var $header = $(".navegation"),
      $clone = $header.before($header.clone().addClass("fixed"));

  $(window).on("scroll", function() {
    var fromTop = $("body").scrollTop();
    $('body').toggleClass("down", (fromTop > 200));
  });
});
</script>

<?php //get_template_part( 'pixel-cource' ); ?>
<?php get_footer(); ?>
