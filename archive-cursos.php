<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package arts_car
 */

get_header(); ?>

<div class="page_course">

  <h1 class="intern">
    <div class="container">Cursos</div>
  </h1>

  <div class="container">
    <?php if ( have_posts() ) : ?>
      <?php
        the_archive_description( '<div class="taxonomy-description">', '</div>' );
      ?>

      <?php while ( have_posts() ) : the_post(); ?>
        <div class="course">
          <div class="col-xs-12 col-md-5">
            <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
            	the_post_thumbnail('');
            }?>
          </div>
          <div class="col-xs-12 col-md-7">
            <?php the_title('<h2>','</h2>'); ?>
            <?php the_content(); ?>
            <a class="main-button main-button-right"  href="<?php echo rwmb_meta( 'ac-courses-link' );?>">Ver mais</a>
          </div>
        </div>

      <?php endwhile; ?>

      <?php the_posts_navigation(); ?>

    <?php else : ?>

      <h1>Não há nenhum serviço cadastrado</h1>

    <?php endif; ?>
  </div>
</div>
<?php get_footer(); ?>
