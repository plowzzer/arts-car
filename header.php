<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package arts_car
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/plkit.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/fonts/fonts.css" />
<!--<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>assets/css/style.css" />-->
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href='https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic' rel='stylesheet' type='text/css'>

<!-- You can use Open Graph tags to customize link previews.
    Learn more: https://developers.facebook.com/docs/sharing/webmasters -->
    <meta property="og:url"           content="<?php echo get_site_url(); ?>" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="<?php echo wp_title(); ?>" />
    <meta property="og:description"   content="<?php echo the_excerpt(); ?>" />
    <meta property="og:image"         content="<?php the_post_thumbnail ?>" />

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/uikit.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/core/modal.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/core/offcanvas.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/components/lightbox.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/components/slideset.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/components/slideshow.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/components/slideshow-fx.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/core/scrollspy.js"></script>

	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=false"></script>


  <!-- Google analytics -->
  <script>

  </script>

  <!-- <script type="text/javascript">
		$(window).load(function() {
			$(".overlay").fadeOut(1500);
		});
	</script> -->

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<nav class="desktop">
	<div class="container">
    <div class="left col-md-4">
      <ul>
        <li class="nav-4 two-lines"><a <?php if ( is_page ( 'quem-somos' ) || is_singular( 'quem-somos' ) ) { ?> class="ative"<?php } ?> href="<?php echo esc_url( home_url( '/quem-somos' ) ); ?>">Quem Somos</a></li>
        <li class="nav-4"><a <?php if ( is_post_type_archive ( 'sericos' ) || is_post_type_archive ( 'servicos' ) ) { ?> class="ative"<?php } ?> href="<?php echo esc_url( home_url( '/servicos' ) ); ?>">Serviços</a></li>
        <li class="nav-4"><a <?php if ( is_post_type_archive ( 'cursos' ) || is_post_type_archive ( 'cursos' ) ) { ?> class="ative"<?php } ?> href="<?php echo esc_url( home_url( '/cursos' ) ); ?>">Cursos</a></li>
        <li class="nav-4"><a <?php if ( is_post_type_archive ( 'clientes' ) || is_post_type_archive ( 'clientes' ) ) { ?> class="ative"<?php } ?> href="<?php echo esc_url( home_url( '/clientes' ) ); ?>">Clientes</a></li>
      </ul>
    </div>
    <div class="col-md-4 logo_space">
      <div class="center">
        <a <?php if ( is_page ( 'home' ) || is_singular( 'home' ) ) { ?> class="ative"<?php } ?> href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo" src="<?=bloginfo('stylesheet_directory')?>/assets/image/logo.png" alt="Arts Car" /></a>
      </div>
    </div>
    <div class=" right col-md-4">
      <ul>
        <li class="nav-3"><a <?php if ( is_page ( 'localizacao' ) || is_singular( 'localizacao' ) ) { ?> class="ative"<?php } ?> href="<?php echo esc_url( home_url( '/localizacao' ) ); ?>">Localização</a></li>
        <li class="nav-3"><a <?php if ( is_page ( 'contato' ) || is_singular( 'contato' ) ) { ?> class="ative"<?php } ?> href="<?php echo esc_url( home_url( '/contato' ) ); ?>">Contato</a></li>
        <li class="nav-3 two-lines">
          <div class="contato">
            <a href="https://www.facebook.com/artscarmartelinhodeourorp"><img id="facebook_nav" src="<?=bloginfo('stylesheet_directory')?>/assets/image/facebook_icon.png" alt="Facebook" /></a>
            <p id="min">Entre em contato</p>
            <p id="tel"><span>16</span> 3289.8328</p>
          </div>
        </li>
      </ul>
    </div>

	</div>
</nav>

<nav class="mobile">
	<div class="container">
    <a <?php if ( is_page ( 'home' ) || is_singular( 'home' ) ) { ?> class="ative"<?php } ?> href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo" src="<?=bloginfo('stylesheet_directory')?>/assets/image/logo.png" alt="Arts Car" /></a>
    <a id="menu" href="#canvasmenu" data-uk-offcanvas>Menu</a>
    <div id="canvasmenu" class="uk-offcanvas">
      <div class="uk-offcanvas-bar">
        <ul>
          <li><a <?php if ( is_page ( 'quem-somos' ) || is_singular( 'quem-somos' ) ) { ?> class="ative"<?php } ?> href="<?php echo esc_url( home_url( '/quem-somos' ) ); ?>">Quem Somos</a></li>
          <li><a <?php if ( is_post_type_archive ( 'sericos' ) || is_post_type_archive ( 'servicos' ) ) { ?> class="ative"<?php } ?> href="<?php echo esc_url( home_url( '/servicos' ) ); ?>">Serviços</a></li>
          <li><a <?php if ( is_post_type_archive ( 'cursos' ) || is_post_type_archive ( 'cursos' ) ) { ?> class="ative"<?php } ?> href="<?php echo esc_url( home_url( '/cursos' ) ); ?>">Cursos</a></li>
          <li><a <?php if ( is_post_type_archive ( 'clientes' ) || is_post_type_archive ( 'clientes' ) ) { ?> class="ative"<?php } ?> href="<?php echo esc_url( home_url( '/clientes' ) ); ?>">Clientes</a></li>
          <li><a <?php if ( is_page ( 'localizacao' ) || is_singular( 'localizacao' ) ) { ?> class="ative"<?php } ?> href="<?php echo esc_url( home_url( '/localizacao' ) ); ?>">Localização</a></li>
          <li><a <?php if ( is_page ( 'contato' ) || is_singular( 'contato' ) ) { ?> class="ative"<?php } ?> href="<?php echo esc_url( home_url( '/contato' ) ); ?>">Contato</a></li>
        </ul>
        <span><a href="https://www.facebook.com/artscarmartelinhodeourorp"><img id="facebook_nav" src="<?=bloginfo('stylesheet_directory')?>/assets/image/facebook_icon.png" alt="Facebook" /></a> Entre em contato: 16 3289.8328</span>
      </div>
    </div>
	</div>
</nav>
