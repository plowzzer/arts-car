<?php
/**
*
*Template Name: Contato
*Template texto: Pagina de Contato
*
* @package arts_car
*/

get_header(); ?>
<div class="page_contact">


  <h1 class="intern">
    <div class="container">Contato</div>
  </h1>

  <div class="contact container">
    <?php $mail_field = rwmb_meta( 'ac-contact-form'); ?>
    <?php echo do_shortcode( $mail_field ); ?>


    <?php
      $contato_image1 = rwmb_meta( 'ac-contato-img01', 'type=image' );
      $contato_image2 = rwmb_meta( 'ac-contato-img02', 'type=image' );
      $contato_image3 = rwmb_meta( 'ac-contato-img03', 'type=image' );
    ?>

    <div class="informations">
      <div class="col-xs-12 col-md-4">
        <div class="single">

          <h3><span>
            <?php foreach ( $contato_image1 as $image ) {
                echo "<img src='{$image['full_url']}' />";
            } ?></span>
            <?php echo rwmb_meta ( 'ac-contato-title01' ); ?>
          </h3>
          <p><?php echo rwmb_meta ('ac-contato-description01'); ?></p>
        </div>
      </div>
      <div class="col-xs-12 col-md-4">
        <div class="single">

          <h3><span>
            <?php foreach ( $contato_image2 as $image ) {
                echo "<img src='{$image['full_url']}' />";
            } ?></span>
            <?php echo rwmb_meta ( 'ac-contato-title02' ); ?>
          </h3>
          <p><?php echo rwmb_meta ('ac-contato-description02'); ?></p>
        </div>
      </div>
      <div class="col-xs-12 col-md-4">
        <div class="single">

          <h3><span>
            <?php foreach ( $contato_image3 as $image ) {
                echo "<img src='{$image['full_url']}' />";
            } ?></span>
            <?php echo rwmb_meta ( 'ac-contato-title03' ); ?>
          </h3>
          <p><?php echo rwmb_meta ('ac-contato-description03'); ?></p>
        </div>
      </div>
    </div>
  </div>

</div>
<?php get_footer(); ?>
