<?php
/**
*
*Template Name: Home
*Template texto: Pagina inicial, usar como index
*
* @package arts_car
*/

get_header(); ?>
<div class="page_home">
  <?php
      $banner01 = rwmb_meta( 'ac-header-img01', 'type=image' );
      $banner02 = rwmb_meta( 'ac-header-img02', 'type=image' );
      $banner03 = rwmb_meta( 'ac-header-img03', 'type=image' );
      $banner04 = rwmb_meta( 'ac-header-img04', 'type=image' );
      $banner05 = rwmb_meta( 'ac-header-img05', 'type=image' );
      $banner06 = rwmb_meta( 'ac-header-img06', 'type=image' );
      $link01 = rwmb_meta( 'ac-header-link01' );
      $link02 = rwmb_meta( 'ac-header-link02' );
      $link03 = rwmb_meta( 'ac-header-link03' );
      $link04 = rwmb_meta( 'ac-header-link04' );
      $link05 = rwmb_meta( 'ac-header-link05' );
      $link06 = rwmb_meta( 'ac-header-link06' );
  ?>

  <!-- Banner Principal -->
  <div class="uk-slidenav-position normal-cover" data-uk-slideshow="{animation: 'scale',autoplay:true}">
      <ul class="uk-slideshow">
      	<?php if( ($banner01)) {
          echo "<li>";
         		foreach ( $banner01 as $image ) {
            if ( ($link01)) {
              echo "<a href='{$link01}'><img src='{$image['full_url']}'></a>";
            } else {
              echo "<img src='{$image['full_url']}'>";
            }
          echo "</li>";
        	}
        } ?>

        <?php if( ($banner02)) {
          echo "<li>";
         		foreach ( $banner02 as $image ) {
            if ( ($link02)) {
              echo "<a href='{$link02}'><img src='{$image['full_url']}'></a>";
            } else {
              echo "<img src='{$image['full_url']}'>";
            }
          echo "</li>";
        	}
        } ?>

        <?php if( ($banner03)) {
          echo "<li>";
         		foreach ( $banner03 as $image ) {
            if ( ($link03)) {
              echo "<a href='{$link03}'><img src='{$image['full_url']}'></a>";
            } else {
              echo "<img src='{$image['full_url']}'>";
            }
          echo "</li>";
        	}
        } ?>

        <?php if( ($banner04)) {
          echo "<li>";
         		foreach ( $banner04 as $image ) {
            if ( ($link04)) {
              echo "<a href='{$link04}'><img src='{$image['full_url']}'></a>";
            } else {
              echo "<img src='{$image['full_url']}'>";
            }
          echo "</li>";
        	}
        } ?>

        <?php if( ($banner05)) {
          echo "<li>";
         		foreach ( $banner05 as $image ) {
            if ( ($link05)) {
              echo "<a href='{$link05}'><img src='{$image['full_url']}'></a>";
            } else {
              echo "<img src='{$image['full_url']}'>";
            }
          echo "</li>";
        	}
        } ?>

        <?php if( ($banner06)) {
          echo "<li>";
         		foreach ( $banner06 as $image ) {
            if ( ($link06)) {
              echo "<a href='{$link06}'><img src='{$image['full_url']}'></a>";
            } else {
              echo "<img src='{$image['full_url']}'>";
            }
          echo "</li>";
        	}
        } ?>
      </ul>
      <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
      <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
      <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom-center uk-text-center">
          <?php if ( ($banner01)) {?>
          	<li data-uk-slideshow-item="0">
          		<a href=""></a>
          	</li>
          <?php } ?>
          <?php if ( ($banner02)) {?>
          	<li data-uk-slideshow-item="1">
          		<a href=""></a>
          	</li>
          <?php } ?>
          <?php if ( ($banner03)) {?>
          	<li data-uk-slideshow-item="2">
          		<a href=""></a>
          	</li>
          <?php } ?>
          <?php if ( ($banner04)) {?>
          	<li data-uk-slideshow-item="3">
          		<a href=""></a>
          	</li>
          <?php } ?>
          <?php if ( ($banner05)) {?>
          	<li data-uk-slideshow-item="4">
          		<a href=""></a>
          	</li>
          <?php } ?>
          <?php if ( ($banner06)) {?>
          	<li data-uk-slideshow-item="5">
          		<a href=""></a>
          	</li>
          <?php } ?>

      </ul>
  </div>

  <h1 class="call"><span>A Artscar</span></h1>
  <?php $sobre_image = rwmb_meta( 'ac-sobre-img', 'type=image' ); ?>
  <?php if( ($sobre_image)) {
      foreach ( $sobre_image as $image ) {
        // echo "<img src='{$image['full_url']}'>";
        echo "<div class='about' style='background-image: url({$image['full_url']})'>";
      }
  } ?>

    <div class="container">
      <div class="col-xs-12 col-md-6">
        <p><?php echo rwmb_meta( 'ac-sobre-texto' ); ?></p>
      </div>
    </div>

  </div>

  <h1 class="call"><span>Serviços</span></h1>
  <div class="services container">
    <?php // WP_Query arguments
      $args = array (
      	'post_type'              => array( 'servicos ' ),
      	'order'                  => 'DESC',
      	'orderby'                => 'none',
      );

      // The Query
      $servicos = new WP_Query( $args );

      // The Loop
      if ( $servicos->have_posts() ) {
      	while ( $servicos->have_posts() ) {
      		$servicos->the_post(); ?>
          <div class="service col-xs-12 col-md-4">
            <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
            	the_post_thumbnail('medium');
            }?>
            <?php the_excerpt(); ?>
            <a class="main-button" href="servicos">Ver Mais</a>

          </div>

      	<?php }
      } else { ?>
      	<h1>Não foi encontrado nenhum serviço cadastrado até o momento</h1>
      <?php }

      // Restore original Post Data
      wp_reset_postdata(); ?>
  </div>


  <h1 class="call"><span>Clientes</span></h1>
  <div class="clients container">
    <?php // WP_Query arguments
      $args = array (
      	'post_type'              => array( 'clientes ' ),
        'nopaging'               => false,
      	'posts_per_page'         => '6',
      	'order'                  => 'DESC',
      	'orderby'                => 'none',
      );

      // The Query
      $servicos = new WP_Query( $args );

      // The Loop
      if ( $servicos->have_posts() ) {
      	while ( $servicos->have_posts() ) {
      		$servicos->the_post(); ?>
          <div class="client col-xs-4 col-md-2">
            <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
            	the_post_thumbnail('medium');
            }?>

          </div>

      	<?php }
      } else { ?>
      	<h1>Não foi encontrado nenhum cliente cadastrado até o momento</h1>
      <?php }

      // Restore original Post Data
      wp_reset_postdata(); ?>
  </div>

  <h1 class="call"><span>Localização</span></h1>
  <div class="map">
    <?php $args = array(
    'type'         => 'map',
    'width'        => '100%', // Map width, default is 640px. You can use '%' or 'px'
    'height'       => '480px', // Map height, default is 480px. You can use '%' or 'px'
    'zoom'         => 14,  // Map zoom, default is the value set in admin, and if it's omitted - 14
    'marker'       => true, // Display marker? Default is 'true',
    'marker_title' => '', // Marker title when hover
    'info_window'  => '<h3>Info Window Title</h3>Info window content. HTML <strong>allowed</strong>', // Info window content, can be anything. HTML allowed.
    );
    echo rwmb_meta( 'map_address', $args ); // For current post?>
  </div>

</div>

<?php get_footer(); ?>
