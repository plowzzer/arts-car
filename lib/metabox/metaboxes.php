<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/
 */

/********************* META BOX DEFINITIONS ***********************/

$prefix = 'ac-';

global $meta_boxes;

$meta_boxes = array();

/* Home - Header */

$meta_boxes[] = array(
  'title'    => 'Banners Header <style>.rwmb-add-file {display: none}</style>',
  'pages' => array( 'page' ),
  'include'  => array(
    'template' => array( 'home.php' ),
  ),
  'id'       => 'pagina-home-banners',
	'fields' => array(
    array(
        'name' => 'Banner 1',
        'id'   => "{$prefix}header-img01",
        'type' => 'plupload_image',
        'max_file_uploads' => 1,
        'columns' => 4,
    ),
    array(
        'name' => 'Banner 2',
        'id'   => "{$prefix}header-img02",
        'type' => 'plupload_image',
        'max_file_uploads' => 1,
        'columns' => 4,
    ),
    array(
        'name' => 'Banner 3',
        'id'   => "{$prefix}header-img03",
        'type' => 'plupload_image',
        'max_file_uploads' => 1,
        'columns' => 4,
    ),
    array(
        'name' => 'Link 1',
        'id'   => "{$prefix}header-link01",
        'type' => 'text',
        'columns' => 4,
    ),
    array(
        'name' => 'Link 2',
        'id'   => "{$prefix}header-link02",
        'type' => 'text',
        'columns' => 4,
    ),
    array(
        'name' => 'Link 3',
        'id'   => "{$prefix}header-link03",
        'type' => 'text',
        'columns' => 4,
    ),
    array(
        'name' => 'Banner 4',
        'id'   => "{$prefix}header-img04",
        'type' => 'plupload_image',
        'max_file_uploads' => 1,
        'columns' => 4,
    ),
    array(
        'name' => 'Banner 5',
        'id'   => "{$prefix}header-img05",
        'type' => 'plupload_image',
        'max_file_uploads' => 1,
        'columns' => 4,
    ),
    array(
        'name' => 'Banner 6',
        'id'   => "{$prefix}header-img06",
        'type' => 'plupload_image',
        'max_file_uploads' => 1,
        'columns' => 4,
    ),
    array(
        'name' => 'Link 4',
        'id'   => "{$prefix}header-link04",
        'type' => 'text',
        'columns' => 4,
    ),
    array(
        'name' => 'Link 5',
        'id'   => "{$prefix}header-link05",
        'type' => 'text',
        'columns' => 4,
    ),
    array(
        'name' => 'Link 6',
        'id'   => "{$prefix}header-link06",
        'type' => 'text',
        'columns' => 4,
    ),
	),
);

// Serviços HOME
$meta_boxes[] = array(
    'title'    => 'Serviços <style>.rwmb-add-file {display: none}</style>',
    'pages' => array( 'page' ),
    'include'  => array(
      'template' => array( 'home.php' ),
    ),
    'id'       => 'home-sobre',
    'fields' => array(
        array(
            'name' => 'Texto',
            'id'   => "{$prefix}sobre-texto",
            'type' => 'textarea',
            'columns' => 6,
        ),
        array(
            'name' => 'Foto',
            'id'   => "{$prefix}sobre-img",
            'type' => 'plupload_image',
            'max_file_uploads' => 1,
            'columns' => 6,
        ),
    )
);

//Map
$meta_boxes[] = array(
	'title'  => 'Mapa - Google Map',
  'pages' => array( 'page' ),
  'include'  => array(
    'template' => array( 'home.php','template-localization.php' ),
  ),
	'fields' => array(
		// Map requires at least one address field (with type = text)
    array(
			'id'   => 'complete_address',
			'name' => 'Endereço completo',
			'type' => 'textarea',
      'columns' => 12,
		),
		array(
			'id'   => 'tomap_address',
			'name' => 'Local no Google Maps',
			'type' => 'text',
			'std'  => 'Ribeirão Preto, Brasil',
      'columns' => 12,
		),
		array(
			'id'            => 'map_address',
			'name'          => 'Map Preview',
			'type'          => 'map',
			// Default location: 'latitude,longitude[,zoom]' (zoom is optional)
			'std'           => '-21.2140076,-47.9582163,11',
			// Name of text field where address is entered. Can be list of text fields, separated by commas (for ex. city, state)
			'address_field' => 'tomap_address',
      'columns' => 12,
		),
	),
);

//servicos
//Galeria de Fotos
$meta_boxes[] = array(
  'title'    => 'Thumb Inside<style>.rwmb-add-file {display: none}</style>',
  'post_types' => 'servicos',
  'id'       => 'services_tag',
	'fields' => array(
		array(
			'id'               => "{$prefix}services-thumb",
			'type'             => 'plupload_image',
			// Delete image from Media Library when remove it from post meta?
			// Note: it might affect other posts if you use same image for multiple posts
			'force_delete'     => false,
			// Maximum image uploads
			'max_file_uploads' => 1,
      'columns' => 12,
		),
	),
);

//Galeria de Fotos
$meta_boxes[] = array(
  'title'    => 'Galeria de Fotos<style>.rwmb-add-file {display: none}</style>',
  'post_types' => 'servicos',
  'id'       => 'services',
	'fields' => array(
		array(
			'id'               => "{$prefix}services-gallery",
			'type'             => 'plupload_image',
			// Delete image from Media Library when remove it from post meta?
			// Note: it might affect other posts if you use same image for multiple posts
			'force_delete'     => false,
			// Maximum image uploads
			'max_file_uploads' => 12,
      'columns' => 12,
		),
	),
);

//Link to on Couses
$meta_boxes[] = array(
  'title'    => 'Link para a Landing Page<style>.rwmb-add-file {display: none}</style>',
  'post_types' => 'cursos',
  'id'       => 'courses',
	'fields' => array(
		array(
			'id'               => "{$prefix}courses-link",
			'type'             => 'text',
      'columns' => 12,
		),
	),
);

//Contact Shortcode
$meta_boxes[] = array(
  'title'    => 'Link para a Landing Page<style>.rwmb-add-file {display: none}</style>',
  'pages' => array( 'page' ),
  'include'  => array(
    'template' => array( 'template-contact.php' ),
  ),
  'id'       => 'contact',
	'fields' => array(
		array(
			'id'               => "{$prefix}contact-form",
			'type'             => 'text',
      'name'             => 'Shortcode para o formulario de contato',
      'columns' => 12,
		),
	),
);

//Contato information fields
$meta_boxes[] = array(
  'title'    => 'Informações<style>.rwmb-add-file {display: none}</style>',
  'pages' => array( 'page' ),
  'include'  => array(
    'template' => array( 'template-contact.php' ),
  ),
  'id'       => 'contact_info',
	'fields' => array(
    array(
        'name' => 'Info Image 1',
        'id'   => "{$prefix}contato-img01",
        'type' => 'image_advanced',
        'max_file_uploads' => 1,
        'columns' => 4,
    ),
    array(
        'name' => 'Info Image 2',
        'id'   => "{$prefix}contato-img02",
        'type' => 'image_advanced',
        'max_file_uploads' => 1,
        'columns' => 4,
    ),
    array(
        'name' => 'Info Image 3',
        'id'   => "{$prefix}contato-img03",
        'type' => 'image_advanced',
        'max_file_uploads' => 1,
        'columns' => 4,
    ),
    array(
        'name' => 'Titulo 1',
        'id'   => "{$prefix}contato-title01",
        'type' => 'text',
        'columns' => 4,
    ),
    array(
        'name' => 'Titulo 2',
        'id'   => "{$prefix}contato-title02",
        'type' => 'text',
        'columns' => 4,
    ),
    array(
        'name' => 'Titulo 3',
        'id'   => "{$prefix}contato-title03",
        'type' => 'text',
        'columns' => 4,
    ),
    array(
        'name' => 'Descrição 1',
        'id'   => "{$prefix}contato-description01",
        'type' => 'textarea',
        'columns' => 4,
    ),
    array(
        'name' => 'Descrição 2',
        'id'   => "{$prefix}contato-description02",
        'type' => 'textarea',
        'columns' => 4,
    ),
    array(
        'name' => 'Descrição 3',
        'id'   => "{$prefix}contato-description03",
        'type' => 'textarea',
        'columns' => 4,
    ),

	),
);

//Galeria de Fotos
$meta_boxes[] = array(
  'title'    => 'Galeria de Fotos<style>.rwmb-add-file {display: none}</style>',
  'pages' => array( 'page' ),
  'include'  => array(
    'template' => array( 'template-about.php' ),
  ),
  'id'       => 'aboutgallery',
	'fields' => array(
		array(
			'id'               => "{$prefix}about-gallery",
			'type'             => 'plupload_image',
			// Delete image from Media Library when remove it from post meta?
			// Note: it might affect other posts if you use same image for multiple posts
			'force_delete'     => false,
			// Maximum image uploads
			'max_file_uploads' => 12,
      'columns' => 12,
		),
	),
);

//Informations
$meta_boxes[] = array(
  'title'    => 'Informações<style>.rwmb-add-file {display: none}</style>',
  'pages' => array( 'page' ),
  'include'  => array(
    'template' => array( 'template-about.php' ),
  ),
  'id'       => 'about_info',
	'fields' => array(
    array(
        'name' => 'Info Image 1',
          'id'   => "{$prefix}about-img01",
        'type' => 'image_advanced',
        'max_file_uploads' => 1,
        'columns' => 6,
    ),
    array(
        'name' => 'Info Image 2',
        'id'   => "{$prefix}about-img02",
        'type' => 'image_advanced',
        'max_file_uploads' => 1,
        'columns' => 6,
    ),
    array(
        'name' => 'Titulo 1',
          'id'   => "{$prefix}about-title01",
        'type' => 'text',
        'columns' => 6,
    ),
    array(
        'name' => 'Titulo 2',
          'id'   => "{$prefix}about-title02",
        'type' => 'text',
        'columns' => 6,
    ),
    array(
        'name' => 'Descrição 1',
          'id'   => "{$prefix}about-description01",
        'type' => 'textarea',
        'columns' => 6,
    ),
    array(
        'name' => 'Descrição 2',
          'id'   => "{$prefix}about-description02",
        'type' => 'textarea',
        'columns' => 6,
    ),
	),
);

// Serviços HOME
$meta_boxes[] = array(
    'title'    => 'Sobre <style>.rwmb-add-file {display: none}</style>',
    'pages' => array( 'page' ),
    'include'  => array(
      'template' => array( 'template-about.php' ),
    ),
    'id'       => 'home-sobre',
    'fields' => array(
        array(
            'name' => 'Texto',
            'id'   => "{$prefix}sobre-texto",
            'type' => 'textarea',
            'columns' => 6,
        ),
        array(
            'name' => 'Foto',
            'id'   => "{$prefix}sobre-img",
            'type' => 'image_advanced',
            'max_file_uploads' => 1,
            'columns' => 6,
        ),
    )
);

//Contact Shortcode
$meta_boxes[] = array(
  'title'    => 'Link para a Landing Page<style>.rwmb-add-file {display: none}</style>',
  'pages' => array( 'page' ),
  'include'  => array(
    'template' => array( 'template-curso1.php' ),
  ),
  'id'       => 'contact_course',
	'fields' => array(
		array(
			'id'               => "{$prefix}contact-form-course",
			'type'             => 'text',
      'name'             => 'Shortcode para o formulario de contato',
      'columns' => 12,
		),
	),
);
/********************* META BOX REGISTERING ***********************/

/**
 * Register meta boxes
 *
 * @return void
 */
function passeio_imovel_metaboxes()
{
	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( !class_exists( 'RW_Meta_Box' ) )
		return;

	global $meta_boxes;
	foreach ( $meta_boxes as $meta_box )
	{
		new RW_Meta_Box( $meta_box );
	}
}
// Hook to 'admin_init' to make sure the meta box class is loaded before
// (in case using the meta box class in another plugin)
// This is also helpful for some conditionals like checking page template, categories, etc.
add_action( 'admin_init', 'passeio_imovel_metaboxes' );
