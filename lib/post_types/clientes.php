<?php
// Register Custom Post Type
function clientes() {

	$labels = array(
		'name'                  => _x( 'Clientes', 'Post Type General Name', 'clientes' ),
		'singular_name'         => _x( 'Cliente', 'Post Type Singular Name', 'clientes' ),
		'menu_name'             => __( 'Clientes', 'clientes' ),
		'name_admin_bar'        => __( 'Clientes', 'clientes' ),
		'parent_item_colon'     => __( 'Cliente pai', 'clientes' ),
		'all_items'             => __( 'Todos os Clientes', 'clientes' ),
		'add_new_item'          => __( 'Adicionar novo Cliente', 'clientes' ),
		'add_new'               => __( 'Adicionar novo', 'clientes' ),
		'new_item'              => __( 'Novo Cliente', 'clientes' ),
		'edit_item'             => __( 'Editar Cliente', 'clientes' ),
		'update_item'           => __( 'Atualizar Cliente', 'clientes' ),
		'view_item'             => __( 'Ver Cliente', 'clientes' ),
		'search_items'          => __( 'Procurar por Cliente', 'clientes' ),
		'not_found'             => __( 'Não encontrado', 'clientes' ),
		'not_found_in_trash'    => __( 'Não encontrado no lixo', 'clientes' ),
		'items_list'            => __( 'Lista de Clientes', 'clientes' ),
		'items_list_navigation' => __( 'Lista de Navegação dos Clientes', 'clientes' ),
		'filter_items_list'     => __( 'Filtro Cliente', 'clientes' ),
	);
	$args = array(
		'label'                 => __( 'Cliente', 'clientes' ),
		'description'           => __( 'Clientes Post Type', 'clientes' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'excerpt', 'thumbnail', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-businessman',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'clientes', $args );

}
add_action( 'init', 'clientes', 0 );
?>
