<?php // Register Custom Post Type
function servicos() {

	$labels = array(
		'name'                  => _x( 'Serviços', 'Post Type General Name', 'services' ),
		'singular_name'         => _x( 'Serviço', 'Post Type Singular Name', 'services' ),
		'menu_name'             => __( 'Serviços', 'services' ),
		'name_admin_bar'        => __( 'Serviços', 'services' ),
		'parent_item_colon'     => __( 'Serviço pai:', 'services' ),
		'all_items'             => __( 'Todos os Serviços', 'services' ),
		'add_new_item'          => __( 'Adicionar novo Serviço', 'services' ),
		'add_new'               => __( 'Adicionar novo', 'services' ),
		'new_item'              => __( 'Novo Serviço', 'services' ),
		'edit_item'             => __( 'Editar Serviço', 'services' ),
		'update_item'           => __( 'Atualizar Serviço', 'services' ),
		'view_item'             => __( 'Ver Serviço', 'services' ),
		'search_items'          => __( 'Procurar Serviços', 'services' ),
		'not_found'             => __( 'Não encontrado', 'services' ),
		'not_found_in_trash'    => __( 'Não encontrado no lixo', 'services' ),
		'items_list'            => __( 'Lista de Serviços', 'services' ),
		'items_list_navigation' => __( 'Lista de navegação de Serviços', 'services' ),
		'filter_items_list'     => __( 'Filtro de Serviços', 'services' ),
	);
	$args = array(
		'label'                 => __( 'Serviço', 'services' ),
		'description'           => __( 'Serviços Post Type', 'services' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-tools',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'servicos', $args );

}
add_action( 'init', 'servicos', 0 ); ?>
