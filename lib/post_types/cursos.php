<?php // Register Custom Post Type
function courses() {

	$labels = array(
		'name'                  => _x( 'Cursos', 'Post Type General Name', 'Courses Post Type' ),
		'singular_name'         => _x( 'Curso', 'Post Type Singular Name', 'Courses Post Type' ),
		'menu_name'             => __( 'Cursos', 'Courses Post Type' ),
		'name_admin_bar'        => __( 'Cursos', 'Courses Post Type' ),
		'parent_item_colon'     => __( 'Curso', 'Courses Post Type' ),
		'all_items'             => __( 'Todos os Cursos', 'Courses Post Type' ),
		'add_new_item'          => __( 'Adicionar novo Curso', 'Courses Post Type' ),
		'add_new'               => __( 'Adicionar novo', 'Courses Post Type' ),
		'new_item'              => __( 'Novo Curso', 'Courses Post Type' ),
		'edit_item'             => __( 'Editar Curso', 'Courses Post Type' ),
		'update_item'           => __( 'Atualizar Curso', 'Courses Post Type' ),
		'view_item'             => __( 'Ver Curso', 'Courses Post Type' ),
		'search_items'          => __( 'Procurar por Curso', 'Courses Post Type' ),
		'not_found'             => __( 'Não encontrado', 'Courses Post Type' ),
		'not_found_in_trash'    => __( 'Não encontrado no lixo', 'Courses Post Type' ),
		'items_list'            => __( 'Lista de Cursos', 'Courses Post Type' ),
		'items_list_navigation' => __( 'Itens de navegação de cursos', 'Courses Post Type' ),
		'filter_items_list'     => __( 'Filtrar lista de cursos', 'Courses Post Type' ),
	);
	$args = array(
		'label'                 => __( 'Curso', 'Courses Post Type' ),
		'description'           => __( 'Courses Post Type', 'Courses Post Type' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-book',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'cursos', $args );

}
add_action( 'init', 'courses', 0 ); ?>
