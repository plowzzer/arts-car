<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package arts_car
 */

get_header(); ?>
  <div class="page_services">


  <h1 class="intern">
    <div class="container">Serviços</div>
  </h1>

  <div class="container">
		<?php if ( have_posts() ) : ?>
			<?php
				the_archive_description( '<div class="taxonomy-description">', '</div>' );
			?>

			<?php while ( have_posts() ) : the_post(); ?>
        <?php $number = $number + 1; ?>

        <div class="service">
          <div class="description">
            <div class="thumb col-xs-12 col-md-4">
              <?php $thumb = rwmb_meta( 'ac-services-thumb', 'type=image' ); ?>
              <?php foreach ( $thumb as $image ) {
                echo "<img src='{$image['full_url']}' '{$image['alt']}' />";
              } ?>
            </div>
            <div class="text col-xs-12 col-md-8">
              <?php the_title('<h2>','</h2>'); ?>
              <h3><?php the_excerpt(); ?></h3>
              <?php the_content(); ?>
            </div>
          </div>

          <?php $gallery = rwmb_meta( 'ac-services-gallery', 'type=image' ); ?>

          <?php if ($gallery): ?>
            <div class="gallery">
              <div class="col-xs-12 col-md-4">
                <h4 class="gallery_call">Galeria de Fotos</h4>
              </div>
              <div class="uk-slidenav-position col-xs-12 col-md-8 no-margin" data-uk-slideset="{default: 4}">

                <ul class="uk-slideset">

                  <?php foreach ( $gallery as $image ) { ?>
                    <li>
                      <a href="<?php echo "{$image['full_url']}"; ?>" data-uk-lightbox="{group:'<?php echo 'gallery',$number; ?>'}" ><?php
                        echo "<img src='{$image['full_url']}' />";
                      echo "</a>"; ?>
                    </li>
                  <?php } ?>
                </ul>
                <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
            </div>
            <div class="gap"></div>
          </div>
            <?php else: ?>
              <div class="gap"></div>
          <?php endif; ?>

        </div>
			<?php endwhile; ?>

		<?php else : ?>

			<h1>Não há nenhum serviço cadastrado</h1>

		<?php endif; ?>
  </div>
</div>

<?php get_footer(); ?>
