<?php
/**
*
*Template Name: Localização
*Template texto: Pagina inicial, usar como index
*
* @package arts_car
*/

get_header(); ?>
<div class="page_location">


  <h1 class="intern">
    <div class="container">Localização</div>
  </h1>

  <div class="map">
    <div class="address">
      <p><?php echo rwmb_meta ( 'complete_address' ); ?></p>
    </div>
    <?php $args = array(
    'type'         => 'map',
    'width'        => '100%', // Map width, default is 640px. You can use '%' or 'px'
    'height'       => '680px', // Map height, default is 480px. You can use '%' or 'px'
    'zoom'         => 14,  // Map zoom, default is the value set in admin, and if it's omitted - 14
    'marker'       => true, // Display marker? Default is 'true',
    'marker_title' => '', // Marker title when hover
    'info_window'  => '<h3>Info Window Title</h3>Info window content. HTML <strong>allowed</strong>', // Info window content, can be anything. HTML allowed.
    );
    echo rwmb_meta( 'map_address', $args ); // For current post?>
  </div>
</div>
<?php get_footer(); ?>
