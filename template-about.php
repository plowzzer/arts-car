<?php
/**
*
*Template Name: Sobre
*Template texto: Pagina de Quem somos
*
* @package arts_car
*/

get_header(); ?>
<div class="page_about">


  <h1 class="intern">
    <div class="container">Quem Somos</div>
  </h1>

  <?php $sobre_image = rwmb_meta( 'ac-sobre-img', 'type=image' ); ?>
  <div class="about">

    <?php if( ($sobre_image)) {
        foreach ( $sobre_image as $image ) {
          // echo "<img src='{$image['full_url']}'>";
          echo "<div class='about' style='background-image: url({$image['full_url']})'>";
        }
    } ?>

      <div class="container">
        <div class="col-xs-12 col-md-6">
          <p><?php echo rwmb_meta( 'ac-sobre-texto' ); ?></p>
        </div>
      </div>
    </div>
  </div>

    <?php
      $contato_image1 = rwmb_meta( 'ac-about-img01', 'type=image' );
      $contato_image2 = rwmb_meta( 'ac-about-img02', 'type=image' );
    ?>

    <div class="gallery">
      <!-- <h4 class="gallery_call">Galeria de Fotos</h4> -->
      <h2 class="gallery_call">Galeria de Fotos</h2>
      <?php $gallery = rwmb_meta( 'ac-about-gallery', 'type=image' ); ?>
      <div class="container">
        <div class="uk-slidenav-position col-xs-12 no-margin" data-uk-slideset="{default: 3}">
          <ul class="uk-slideset">

            <?php foreach ( $gallery as $image ) { ?>
              <li>
                <a href="<?php echo "{$image['full_url']}"; ?>" data-uk-lightbox="{group:'about'}" ><?php
                  echo "<img src='{$image['full_url']}' />";
                echo "</a>"; ?>
              </li>
            <?php } ?>
          </ul>
          <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
          <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
        </div>
      </div>
    </div>

    <div class="informations container">
      <div class="col-xs-12 col-md-6">
        <div class="single">

          <h3><span>
            <?php foreach ( $contato_image1 as $image ) {
                echo "<img src='{$image['full_url']}' />";
            } ?></span>
            <?php echo rwmb_meta ( 'ac-about-title01' ); ?>
          </h3>
          <p><?php echo rwmb_meta ('ac-about-description01'); ?></p>
        </div>
      </div>
      <div class="col-xs-12 col-md-6">
        <div class="single">

          <h3><span>
            <?php foreach ( $contato_image2 as $image ) {
                echo "<img src='{$image['full_url']}' />";
            } ?></span>
            <?php echo rwmb_meta ( 'ac-about-title02' ); ?>
          </h3>
          <p><?php echo rwmb_meta ('ac-about-description02'); ?></p>
        </div>
      </div>
    </div>
  </div>

</div>
<?php get_footer(); ?>
