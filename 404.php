<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package arts_car
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header container">
					<h1 class="page-title">Erro 404</h1>
					<p>Pagina não encontrada</p>
				</header><!-- .page-header -->


		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
