<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package arts_car
 */

get_header(); ?>

<div class="page_cursos">

  <h1 class="intern">
    <div class="container">Clientes</div>
  </h1>

  <div class="container">
    <?php if ( have_posts() ) : ?>
      <p>Realizamos diversos atendimentos com excelência, criando uma lista de respeitáveis parceiros, confira abaixo nossos principais clientes:</p>
      <div class="clients">
      <?php while ( have_posts() ) : the_post(); ?>

          <div class="client col-xs-4 col-md-3">
            <div class="rounded">
              <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
              	the_post_thumbnail('');
              }?>
            </div>
          </div>

      <?php endwhile; ?>

      </div>

    <?php else : ?>

      <h1>Não há nenhum cliente cadastrado</h1>

    <?php endif; ?>
  </div>
</div>
<?php get_footer(); ?>
